#include "hex.hpp"
using namespace std;



hex::hex()
{
	loc.x = 0.;
	loc.y = 0.;
	getVerts(loc, vertices);
	return;
}


hex::makeOrigin()
{
	loc.x = 0.;
	loc.y = 0.;
	getVerts(loc, vertices);
	for(int i=0; i<6;i++)
	{
		if(i != 3)
			&neighbors[i] = &neighbors[3];
	}
		
}

int hex::dir2side(float dir)
{
	return (dir*pi/3)%6;
}

int hex::flipSide(int side)
{
	return (side+3)%6;
}

void hex::surfState(int i, bool& sides)
{
	vert POI, VOI;
	a = (i*pi/3);
	POI.x = loc.x + 2*cos(a);
	POI.y = loc.y + 2*sin(a);
	for(int j=1;j<6;j++)
	{
		b = ((j+flipside(i))*pi/3);
		VOI.x = POI.x + cos(b);
		VOI.y = POI.y + sin(b);
		sides[(j+i)%6] = connected(VOI, O)
	}
}

void hex::drawMe(vec3 fill, vec3 line)
{
	glColor3f(fill.x,fill.y,fill.z);
    glBegin(GL_POLYGON);
	for(int i=0;i<6;i++)
		glVertex2i(vertices[i].x,vertices[i].y);
    glEnd();
	glColor3f(line.x,line.y,line.z);
	glBegin(GL_LINE_LOOP);
	for(int i=0;i<6;i++)
		glVertex2i(vertices[i].x,vertices[i].y);
    glEnd();
    glFlush();
}



