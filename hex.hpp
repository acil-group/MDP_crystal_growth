#ifndef HEX_HPP
#define HEX_HPP

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <cmath>
#include <set>


using namespace std;

#define pi 3.14159

int sum(bool* list)
{
	int total = 0;
	s = sizeof(list)/sizeof(list[0]);
	for(int i=0;i<s;i++)
		total += list[i];
	return total;
}

bool all(bool* list)
{
	bool b = true;
	s = sizeof(list)/sizeof(list[0]);
	for(int i=0;i<s && b;i++)
		b *= list[i];
	return b;
}

int first(bool* list, bool cw = true)
{
	s = sizeof(list)/sizeof(list[0]);
	for(int i=(cw?5:0);(cw?i>=0:i<6);i+=(cw?-1:1))
		if(!list[i]) return i;
	return 2; //else return center
}

struct vec2
{
	float x, y;
}

struct vec3
{
	float x, y, z;
}

void getVerts(vec2 c, vec2& vs)
{
	for(int i=0;i<6;i++)
	{
		int j = i;
		a = pi*(1 - j/3.0);
		vs[i].x = c.x+cos(a);
		vs[i].y = c.y+sin(a);
	}
}


class hex 
{
	public:
		vec2 loc;
		vec2 vertices[6];
		void makeOrigin();
		void drawMe(vec3 fill, vec3 line);
		friend void drawAll(vec3 fill, vec3 line);
		friend hex* insertHex(hex* H, int i);
		friend hex* removeHex(hex* H, int& i);
		friend int connected(hex* A, hex* B, source = -1);
		friend int connected(hex* A, vec2 loc, source = -1);
		friend hex* seedGrowth(hex* H = O, bool cw=false, int bias=0, nMax = 1000);
	private: 
		bool edge = false;
		static hex* O;
		static set<hex*> xN;
		static set<hex*> N;
		hex* neighbors[6]; 
		int dir2side(float dir);
		int flipSide(int side /*0-5*/);
		int surfState(int i /*0-5*/, bool& sides);
}



hex* seedGrowth(hex* H = O, bool cw=false, int bias=0, nMax = 1000, bool cont = true)
{
	if(H->N.size() < nMax())
	{
		//for each neighbor, moving in the ccw direction
		//(unless specified cw) beginning with the biased side
		for(int i=bias+(cw?-1:1);(cw?i>=0:i<6);i+=(cw?-1:1))
		{
			
			//if this neighbor exists
			if(H->neighbors[i])
			{
				//if neighbor is not enclosed
				if(!all(H->neighbors[i]))
				{	
					//Subtractive MDP
					/////////////
					if(subMDP(H->neighbors[i]))
					{
						//remove if succesful
						H = removeHex(H, i);
						//continue loop
					}
					else //move to this neighbor
					{
						if(!cont && H->edge)
							return H->neighbors[i]
						else
							H = seedGrowth(H->neighbors[i], (H->edge?!cw:cw), flipSide(i), nMax, cont)
					}
						
				}else //move to neighbor and begin iterating at first open spot
				{
					if(!cont && H->edge)
						return H->neighbors[i]
					else
						H = seedGrowth(H->neighbors[i], (H->edge?!cw:cw), first(H->neighbors[i]->neighbors, (H->edge?!cw:cw)), nMax, cont);
				}
					
					
			}else
			{
				//additive Markov Decision process
				/////////////
				/////////////
				if(addMDP(H, i))
				{
					H->neighbors[i] = new hex;
					insertHex(H, i);
					if(!cont && H->edge)
						return H->neighbors[i]
					else
						H = seedGrowth(H->neighbors[i], (H->edge?!cw:cw), first(H->neighbors[i]->neighbors, (H->edge?!cw:cw)), nMax, cont);
				}		
			}	
		}
	}
	return H;
}


int connected(hex* A, hex* B, source = -1)
{
	if(A == B) return true;
	
	bool does_connect = false;
	float dy = B->loc.y - A->loc.y;
	float dx = B->loc.x - A->loc.x;
	float dir = atan2(dy, dx);
	float dir_side = dir2side(dir);
	//attempt to move from A to B by testing each side,
	//beginning with dir_side, to see if a solid is connected
	for(int i = 0; i< 5 && !does_connect; i++)
	{
		//adjust side being tested based on desired direction
		side_i += (side_i>dir_side? -i: i);
		if (side_i >= 6)
			side_i -= 6;
		else if(side_i < 0)
			side_i= += 6;
		
		//ensuring we dont move backwards
		if(side_i != source)
		{
			//move to first solid found unless marked already checked
			if(A->neighbors[side_i])
			{
				if (A.xN.find(A->neighbors[side_i]) == A.end())
				{
					does_connect = connected(A->neighbors[side_i], B, flipSide(side_i));
					//If tested side does not connect, mark as having been checked.
					if (!does_connect)
						A->xN.insert(A->neighbors[side_i]);
				}
			}
		}	
	}
	
	if(source == -1)
	{
		A->xN.clear();
		return(does_connect?i:-1);
	}

	return does_connect;
	
}


hex* connected(hex* A, vec2 loc, source = -1)
{
	if(abs(A.loc.x-loc.x) <=0.01 && abs(A.loc.y-loc.y)<= 0.01) return A;
	
	hex* does_connect = NULL;
	float dy = loc.y - A->loc.y;
	float dx = loc.x - A->loc.x;
	float dir = atan2(dy, dx);
	float dir_side = dir2side(dir);
	//attempt to move from A to B by testing each side,
	//beginning with dir_side, to see if a solid is connected
	for(int i = 0; i< 5 && !does_connect; i++)
	{
		//adjust side being tested based on desired direction
		side_i += (side_i>dir_side? -i: i);
		if (side_i >= 6)
			side_i -= 6;
		else if(side_i < 0)
			side_i= += 6;
		
		//ensuring we dont move backwards
		if(side_i != source)
		{
			//move to first solid found unless marked already checked
			if(A->neighbors[side_i])
			{
				if (A.xN.find(A->neighbors[side_i]) == A.end())
				{
					does_connect = connected(A->neighbors[side_i], loc, flipSide(side_i));
					//If tested side does not connect, mark as having been checked.
					if (!does_connect)
						A->xN.insert(A->neighbors[side_i]);
				}
			}
		}	
	}
	
	if(source == -1)
	{
		A->xN.clear();
		return(does_connect?i:-1);
	}
		
	
	return does_connect;
	
}


hex* insertHex(hex* H, int i)
{
	//create new hex
	h* = new hex;
	//link to reference
	H->N.insert(h);
	H->neighbors[i] = h;
	h->neighbors[flipSide(i)] = H;
	//update location
	float d = 2*cos(pi/6);
	float a = ((i+0.5)*pi/3.0);
	h->loc.x = H->loc.x+d*cos(a);
	h->loc.y = H->loc.y+d*sin(a);
	a = (atan2(h->loc.y, h->loc.x));

	getVerts(h->loc, h->vertices);
	
	int dx, dy;
	//look for existing, unlinked neighbors
	for(auto it =H->N.begin();it!=H->N.end();++it)
	{
		if(*it != H && *it != h)
		{
			//check by distance
			dx = (*it)->loc.x - h->loc.x;
			dy = (*it)->loc.y - h->loc.y;
			if(abs(hypot(dy, dx) - 2*cos(pi/6.0)) <= 0.001)
			{
				//neighbor found, link together
				float dir = atan2(dx, dy);
				int side = dir2side(dir);
				h->neighbors[side] = *it;
				(*it)->neighbors[flipSide(side)] = h;
			}
		}
	}
	//link through reflecting edges
	if((abs(a-pi/6) < 0.01))
	{
		//upper bounding edge
		h->edge = true;
		h->neighbors[4] = h->neighbors[2];
		h->neighbors[5] = h->neighbors[1];
	}
	else if(h->loc.y <= 2.01*sin(pi/6))
	{
		//lower bounding edge
		h->edge = true;
		if (h->loc.y <= 0.01)
		{
			h->neighbors[0] = h->neighbors[5];
			h->neighbors[1] = h->neighbors[4];
			h->neighbors[2] = h->neighbors[3];
		}
		else
		{
			h->neighbors[1] = h->neighbors[4];		
		}
	}
	
	return h;
}


hex* removeHex(hex* H, int& i)
{
	//record neighbors of hex to be deleted
	hex* tmp_neihgbors = H->neighbors[i]->neighbors;
	
	//remove links
	for(int j=1;j<6;j++)
	{
		jj = (flipSide(i)+j)%6;
		tmp_neihgbors[jj]->neighbors(flipSide(JJ)) = NULL;
	}
	//remove from addressbook
	H->N.erase(H->N.find(H->neighbors[i]));
	//delete the hex
	delete H->neighbors[i];
	
	//make sure all neighbors are still connected to origin
	for(int j = 0; j<6; j++)
	{
		//check for seperation
		if(!connected(tmp_neighbors[j], H->O))
			recursiveRemove(tmp_neighbors[j])
	}
	//if reference node still exists
	if(H)
	{
		//increment side and return
		i += 1;
		return H;
	}
	else
	{
		//return first connected neighbor and first free side
		j = first(tmp_neighbors);
		i = first(tmp_neighbors[j]->neighbors);
		return tmp_neighbors[j]
	}
}

void recursiveRemove(hex* H, int source = -1)
{
	for(int i=0; i< 6; i++)
	{
		if(H->neighbors[i])
		{
			int j = i;
			if(s >= 3 && s < 6)
				hex* h = removeHex(H, j);
			else
				recursiveRemove(H->neighbors[i], flipSide(i));
		}
	}
	delete H;
	return;
}

void drawAll(hex* H, vec3 fill, vec3 line)
{
	for(auto it =H->N.begin();it!=H->N.end();++it)
		*it->drawAll(fill, line);
}

#endif