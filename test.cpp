#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cmath>
using namespace std;

float cam_x = 0.0;
float cam_y = 0.0;
float cam_z = 5.0;



clock_t t0, t1;
float t_max = 30;
float dt;

void init(void)
{
	glClearColor(0.0,0.0,0.0,0.0);
	glShadeModel(GL_FLAT);
}

static void time_func(int x)
{
	cout<<cam_x<<endl;
	cam_x = 5.0*sin((2.0*dt/60.0)*3.14159);
	//cam_z = 5.0*cos((2.0*dt/30.0)*3.14159);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cam_x,cam_y,cam_z,0.0,0.0,0.0,0.0,1.0,0.0);
	glutPostRedisplay();
	glutTimerFunc(10, time_func, 0);
}

void display(void)
{
	t1 = clock();
	dt = (float)(t1-t0) / CLOCKS_PER_SEC;
	if(dt > t_max)
		t0 = t1;
	glClear(GL_COLOR_BUFFER_BIT);
	glutWireCube(1.0);
	glutSwapBuffers();
}

void reshape(int w, int h)
{
	glViewport(0,0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 20.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(cam_x,cam_y,cam_z,0.0,0.0,0.0,0.0,1.0,0.0);
}

int main(int argc, char** argv)
{
	char a;
	t0 = clock();
	
	cout<<"Hello World\n"<<endl;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(100,100);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(10, time_func, 0);
	glutMainLoop();
	return 0;
}
